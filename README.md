# Go Docker
Go app inside Docker.

Taken from [https://www.callicoder.com/docker-golang-image-container-example/]

## Instructions
### Go modules locally
Activate Go modules inside $GOPATH (Add to ~/.zshrc)
```
export GO111MODULE=on
```
Then:
```
$ go mod init 
$ go build
$ ./go-docker
2019/04/03 22:15:51 Starting Server
```

### Run inside Docker
Build
```
docker build -t go-docker .
```
Check info
```
# List all the images
docker image ls
# List running containers
docker container ls
```
Run the Docker image
```
docker run -d -p 8080:8080 go-docker
```
Interacting with the app
```
$ curl http://localhost:8080?name=Miguel
Hello, Miguel
```
Stop container
```
docker container stop <Container ID>
```
